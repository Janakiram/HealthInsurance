package com.insurance.dto;

public class Habits {

	private String smoking;
	private String alcohol;
	private String excercise;
	private String drugs;
	
	public String getSmoking() {
		return smoking;
	}
	public void setSmoking(String smoking) {
		this.smoking = smoking;
	}
	public String getAlcohol() {
		return alcohol;
	}
	public void setAlcohol(String alcohol) {
		this.alcohol = alcohol;
	}
	public String getExcercise() {
		return excercise;
	}
	public void setExcercise(String excercise) {
		this.excercise = excercise;
	}
	public String getDrugs() {
		return drugs;
	}
	public void setDrugs(String drugs) {
		this.drugs = drugs;
	}
}
