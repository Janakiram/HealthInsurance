package com.insurance.dto;

public class CurrentHealth 
{
	private String hyperTension;
	
	private double bloodPressure;
	
	private double bloodSugar;
	
	private int overWeight;

	public String getHyperTension() {
		return hyperTension;
	}

	public void setHyperTension(String hyperTension) {
		this.hyperTension = hyperTension;
	}

	public double getBloodPressure() {
		return bloodPressure;
	}

	public void setBloodPressure(double bloodPressure) {
		this.bloodPressure = bloodPressure;
	}

	public double getBloodSugar() {
		return bloodSugar;
	}

	public void setBloodSugar(double bloodSugar) {
		this.bloodSugar = bloodSugar;
	}

	public int getOverWeight() {
		return overWeight;
	}

	public void setOverWeight(int overWeight) {
		this.overWeight = overWeight;
	}
	

}
